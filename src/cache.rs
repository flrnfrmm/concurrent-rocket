use std::collections::{HashMap, VecDeque};
use std::sync::{Arc, RwLock};
use std::thread;

pub fn spawn_thread(book_reviews: Arc<RwLock<HashMap<String, VecDeque<i32>>>>) {
    thread::spawn(move || loop {
        if let Ok(mut write_guard) = book_reviews.write() {
            let s = "key".to_string();
            let queue = write_guard.entry(s).or_insert_with(|| VecDeque::new());
            if queue.len() > 10 {
                queue.pop_front();
            }
            queue.push_back(rand::random::<i32>());
        }
        thread::sleep(std::time::Duration::from_millis(3000));
    });
}
