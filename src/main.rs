#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

mod cache;

use rocket::State;
use std::collections::{HashMap, VecDeque};
use std::sync::{Arc, RwLock};

#[get("/")]
fn index(book_reviews: State<Arc<RwLock<HashMap<String, VecDeque<i32>>>>>) -> String {
    let mut resp: String = "".into();
    if let Ok(read_guard) = book_reviews.read() {
        for (k, vs) in (*read_guard).iter() {
            resp += &k.clone();
            for v in vs {
                let s = format!("\n{}", v);
                resp.push_str(&s);
            }
        }
    }
    resp
}

fn main() {
    let book_reviews: HashMap<String, VecDeque<i32>> = HashMap::new();

    let lock = Arc::new(RwLock::new(book_reviews));
    cache::spawn_thread(lock.clone());

    rocket::ignite()
        .mount("/", routes![index])
        .manage(lock)
        .launch();
}
